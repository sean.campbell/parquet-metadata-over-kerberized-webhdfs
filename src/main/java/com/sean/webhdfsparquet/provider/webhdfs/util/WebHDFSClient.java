package com.sean.webhdfsparquet.provider.webhdfs.util;

import com.sean.webhdfsparquet.util.UserGroupInformationAdapter;
import io.vavr.control.Try;
import org.apache.commons.io.IOUtils;
import org.apache.hadoop.security.authentication.client.AuthenticatedURL;
import parquet.bytes.BytesUtils;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.PrivilegedAction;
import java.util.function.Function;

public class WebHDFSClient {
    private final UserGroupInformationAdapter adapter;

    public WebHDFSClient(final UserGroupInformationAdapter adapter) {
        this.adapter = adapter;
    }

    public String getAsString(final String url) {
        return Try.of(() -> IOUtils.toString(getAsStream(url)))
                .getOrElseThrow((Function<Throwable, RuntimeException>) RuntimeException::new);
    }

    public int getAsInt(final String url) {
        return Try.of(() -> BytesUtils.readIntLittleEndian(getAsStream(url)))
                .getOrElseThrow((Function<Throwable, RuntimeException>) RuntimeException::new);
    }

    public InputStream getAsStream(final String url) {
        return getAsTriedInputStream(url)
                .getOrElseThrow(t -> new RuntimeException(String.format("Unable to create inputstream for %s!", url), t));
    }

    private Try<InputStream> getAsTriedInputStream(final String url) {
        final PrivilegedAction<HttpURLConnection> action = () ->
                Try.of(() -> new AuthenticatedURL().openConnection(new URL(url), new AuthenticatedURL.Token()))
                .getOrElseThrow(t -> new RuntimeException("Unable to open Kerberized connection!", t));

        return Try.of(() -> (InputStream) adapter.getUgi().doAs(action).getContent());
    }
}
