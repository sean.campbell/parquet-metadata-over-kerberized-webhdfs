package com.sean.webhdfsparquet.provider.webhdfs;

import org.apache.spark.sql.types.DataType;

public interface SchemaProvider {
    DataType getSchema(final String path);
}
