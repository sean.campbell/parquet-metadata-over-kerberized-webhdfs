package com.sean.webhdfsparquet.provider.webhdfs.util;

import org.apache.hadoop.fs.PositionedReadable;
import org.apache.hadoop.fs.Seekable;

import java.io.ByteArrayInputStream;
import java.io.IOException;

public class SeekableByteArrayInputStream extends ByteArrayInputStream implements Seekable, PositionedReadable {

    public SeekableByteArrayInputStream(final byte[] data) {
        super(data);
    }

    @Override
    public int read(final long position, final byte[] buffer, final int offset, final int length) {
        if (position >= buf.length || position + length > buf.length || length > buffer.length) {
            throw new IllegalArgumentException();
        }
        System.arraycopy(buf, (int)position, buffer, offset, length);
        return length;
    }

    @Override
    public void readFully(final long position, final byte[] buffer, final int offset, int length) throws IOException {
        read(position, buffer, offset, length);
    }

    @Override
    public void readFully(final long position, final byte[] buffer) {
        read(position, buffer, 0, buffer.length);
    }

    @Override
    public void seek(long pos) throws IOException {
        if (mark != 0) {
            throw new IllegalStateException();
        }
        reset();
        long skipped = skip(pos);

        if (skipped != pos) {
            throw new IOException();
        }
    }

    @Override
    public long getPos() {
        return pos;
    }

    @Override
    public boolean seekToNewSource(final long l) {
        throw new UnsupportedOperationException("Not supported!");
    }
}
