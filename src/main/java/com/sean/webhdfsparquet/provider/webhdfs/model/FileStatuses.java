package com.sean.webhdfsparquet.provider.webhdfs.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import java.util.List;

@JsonDeserialize(builder = FileStatuses.FileStatusesBuilder.class)
public class FileStatuses {
    private final List<FileStatus> fileStatus;

    FileStatuses(final List<FileStatus> FileStatus) {
        this.fileStatus = FileStatus;
    }

    public static FileStatuses.FileStatusesBuilder builder(final List<FileStatus> fileStatuses) {
        return new FileStatusesBuilder(fileStatuses);
    }

    public List<FileStatus> getFileStatus() {
        return fileStatus;
    }

    @JsonPOJOBuilder(withPrefix = "")
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class FileStatusesBuilder {
        private List<FileStatus> fileStatus;

        FileStatusesBuilder(@JsonProperty("FileStatus") final List<FileStatus> fileStatus) {
            this.fileStatus = fileStatus;
        }

        public FileStatusesBuilder FileStatus(@JsonProperty("FileStatus") final List<FileStatus> fileStatus) {
            this.fileStatus = fileStatus;
            return this;
        }

        public FileStatuses build() {
            return new FileStatuses(fileStatus);
        }
    }
}
