package com.sean.webhdfsparquet.provider.webhdfs.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@JsonDeserialize(builder = FileStatus.FileStatusBuilder.class)
public class FileStatus {
    private final String pathSuffix;
    private final String type;
    private final long modificationTime;
    private final long length;

    @JsonPOJOBuilder(withPrefix = "")
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class FileStatusBuilder{
    }
}
