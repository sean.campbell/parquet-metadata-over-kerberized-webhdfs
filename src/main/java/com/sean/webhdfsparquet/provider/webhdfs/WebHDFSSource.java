package com.sean.webhdfsparquet.provider.webhdfs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import com.sean.webhdfsparquet.provider.webhdfs.model.FileStatus;
import com.sean.webhdfsparquet.provider.webhdfs.model.FileStatusesWrapper;
import com.sean.webhdfsparquet.provider.webhdfs.util.ParquetMetadataReader;
import com.sean.webhdfsparquet.provider.webhdfs.util.WebHDFSClient;
import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.control.Try;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.StructType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import parquet.hadoop.metadata.ParquetMetadata;

import javax.ws.rs.core.UriBuilder;
import java.util.Map;
import java.util.Objects;

import static io.vavr.API.*;
import static io.vavr.Patterns.$Failure;
import static io.vavr.Patterns.$Success;

public class WebHDFSSource implements SchemaProvider {
    private static final Logger LOGGER = LoggerFactory.getLogger(WebHDFSSource.class);
    private static final String START_FILE_NAME = "part", PARQUET_TYPE = "parquet";

    private static final String OP = "op", LIST_STATUS = "LISTSTATUS", OPEN = "OPEN", OFFSET = "offset";

    private static final Map<String, Integer> MEDIUM_MAP = ImmutableMap.of("hdfs:", 5, "s3:", 3);
    private static final String SQL_SCHEMA_KEY = "org.apache.spark.sql.parquet.row.metadata";

    private final WebHDFSClient client;
    private final String base;
    private final ParquetMetadataReader metadataReader;
    private final ObjectMapper mapper;

    public WebHDFSSource(final WebHDFSClient client, final String base, final ObjectMapper mapper) {
        this(client, base, new ParquetMetadataReader(), mapper);
    }

    WebHDFSSource(final WebHDFSClient client, final String base, final ParquetMetadataReader metadataReader, final ObjectMapper mapper) {
        this.client = Objects.requireNonNull(client);
        this.base = Objects.requireNonNull(base);
        this.metadataReader = Objects.requireNonNull(metadataReader);
        this.mapper = Objects.requireNonNull(mapper);
    }

    @Override
    public DataType getSchema(final String path) {
        final String fpath = stripMedium(path);
        LOGGER.info("Fetching metadata for path:{}", fpath);

        final ParquetMetadata metadata = readMetadata(fpath);

        if (null == metadata || null == metadata.getFileMetaData()) {
            throw new RuntimeException(String.format("Parquet metadata is null or empty for path:%s", fpath));
        }

        final Map<String, String> ref = metadata.getFileMetaData().getKeyValueMetaData();
        if (null == ref || ref.isEmpty() || !ref.containsKey(SQL_SCHEMA_KEY)) {
            throw new RuntimeException(String.format("Unable to obtain SQL schema from parquet metadata for path:%s", fpath));
        }

        final DataType dt = DataType.fromJson(ref.get(SQL_SCHEMA_KEY));
        if (dt instanceof StructType) {
            return dt;
        } else {
            throw new RuntimeException(String.format("The datatype is not a struct type for path:%s", fpath));
        }
    }

    private ParquetMetadata readMetadata(final String path) {
        final Tuple2<String, Long> nameWithLength = getLatestFileListing(path);

        final String latestFile = UriBuilder.fromUri(base).path(path).path(nameWithLength._1)
                .build().toString();
        final int length = nameWithLength._2.intValue();

        final String footerLengthLocation = UriBuilder.fromUri(latestFile).queryParam(OP, OPEN)
                .queryParam(OFFSET, metadataReader.getFooterLengthPosition(length))
                .build().toString();
        final int footerLength = client.getAsInt(footerLengthLocation);

        final String fromFooterStart = UriBuilder.fromUri(latestFile).queryParam(OP, OPEN)
                .queryParam(OFFSET, metadataReader.getFooterStartPosition(footerLength, length))
                .build().toString();

        return metadataReader.readMetadata(client.getAsStream(fromFooterStart));
    }


    private Tuple2<String, Long> getLatestFileListing(final String path) {
        final String uri = UriBuilder.fromUri(base).path(path)
                .queryParam(OP, LIST_STATUS)
                .build().toString();

        final String resp = client.getAsString(uri);

        final Try<FileStatus> tryReduce = Try.of(() -> mapper.readValue(resp, FileStatusesWrapper.class)
                .getFileStatuses().getFileStatus().stream()
                .filter(a -> a.getPathSuffix().startsWith(START_FILE_NAME) || a.getPathSuffix().contains(PARQUET_TYPE))
                .reduce((a, b) -> Long.compare(a.getModificationTime(), b.getModificationTime()) <= 0 ? a : b)
                .get()
        );

        return Match(tryReduce).of(
                Case($Success($()), obj -> Tuple.of(obj.getPathSuffix(), obj.getLength())),
                Case($Failure($()), e -> {throw new RuntimeException(e);})
        );
    }

    private static String stripMedium(final String path) {
        return MEDIUM_MAP.entrySet().stream()
                .filter(elem -> path.startsWith(elem.getKey()))
                .map(elem -> path.substring(elem.getValue()))
                .findFirst()
                .orElse(path);
    }
}
