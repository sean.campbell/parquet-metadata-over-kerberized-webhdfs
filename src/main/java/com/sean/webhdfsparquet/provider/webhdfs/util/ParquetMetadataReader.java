package com.sean.webhdfsparquet.provider.webhdfs.util;

import com.google.common.io.ByteStreams;
import io.vavr.control.Try;
import org.apache.hadoop.fs.FSDataInputStream;
import parquet.format.converter.ParquetMetadataConverter;
import parquet.hadoop.metadata.ParquetMetadata;

import java.io.InputStream;

import static parquet.hadoop.ParquetFileWriter.MAGIC;

public class ParquetMetadataReader {
    private static final int FOOTER_LENGTH_SIZE = 4;

    private final ParquetMetadataConverter converter;

    public ParquetMetadataReader() {
        this.converter = new ParquetMetadataConverter();
    }

    public int getFooterLengthPosition(final int length) {
        if (length < MAGIC.length + FOOTER_LENGTH_SIZE + MAGIC.length) {
            throw new RuntimeException("The input cannot be a Parquet file (too small)!");
        }
        return length - FOOTER_LENGTH_SIZE - MAGIC.length;
    }

    public int getFooterStartPosition(final int footerLength, final int length) {
        final int footerLengthPosition = getFooterLengthPosition(length);
        final int footerStartPosition = footerLengthPosition - footerLength;
        if (footerStartPosition <= MAGIC.length || footerStartPosition >= footerLengthPosition) {
            throw new RuntimeException("Corrupted file: the footer index is not within the file!");
        }
        return footerStartPosition;
    }

    public ParquetMetadata readMetadata(final InputStream is) {
        final byte[] data = Try.of(() -> ByteStreams.toByteArray(is))
                .getOrElseThrow(t -> new RuntimeException());
        final FSDataInputStream fis = new FSDataInputStream(new SeekableByteArrayInputStream(data));
        return Try.of(() -> converter.readParquetMetadata(fis))
                .andFinallyTry(fis::close)
                .getOrElseThrow(t -> new RuntimeException("Unable to read Parquet metadata!"));
    }
}
