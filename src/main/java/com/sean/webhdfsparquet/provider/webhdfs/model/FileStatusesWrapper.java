package com.sean.webhdfsparquet.provider.webhdfs.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = FileStatusesWrapper.FileStatusesWrapperBuilder.class)
public class FileStatusesWrapper {
    private final FileStatuses FileStatuses;

    FileStatusesWrapper(final FileStatuses FileStatuses) {
        this.FileStatuses = FileStatuses;
    }

    public static FileStatusesWrapperBuilder builder(final FileStatuses FileStatuses) {
        return new FileStatusesWrapperBuilder(FileStatuses);
    }

    public FileStatuses getFileStatuses() {
        return FileStatuses;
    }

    @JsonPOJOBuilder(withPrefix = "")
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class FileStatusesWrapperBuilder {
        private FileStatuses FileStatuses;

        FileStatusesWrapperBuilder(@JsonProperty("FileStatuses") final FileStatuses FileStatuses) {
            this.FileStatuses = FileStatuses;
        }

        public FileStatusesWrapperBuilder FileStatuses(@JsonProperty("FileStatuses") final FileStatuses FileStatuses) {
            this.FileStatuses = FileStatuses;
            return this;
        }

        public FileStatusesWrapper build() {
            return new FileStatusesWrapper(FileStatuses);
        }
    }
}
