package com.sean.webhdfsparquet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebhdfsParquetApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebhdfsParquetApplication.class, args);
	}

}

