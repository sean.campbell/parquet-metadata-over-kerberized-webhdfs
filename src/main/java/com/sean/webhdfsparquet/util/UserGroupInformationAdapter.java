package com.sean.webhdfsparquet.util;

import io.vavr.control.Try;
import org.apache.directory.server.kerberos.shared.crypto.encryption.KerberosKeyFactory;
import org.apache.directory.server.kerberos.shared.keytab.Keytab;
import org.apache.directory.server.kerberos.shared.keytab.KeytabEntry;
import org.apache.directory.shared.kerberos.KerberosTime;
import org.apache.directory.shared.kerberos.codec.types.EncryptionType;
import org.apache.directory.shared.kerberos.components.EncryptionKey;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.security.UserGroupInformation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class UserGroupInformationAdapter {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserGroupInformationAdapter.class);
    private static final String JAVA_SECURITY_KRB5_CONF = "java.security.krb5.conf";

    private final UserGroupInformation ugi;

    public UserGroupInformationAdapter(final String userId, final String pass, final String realm) {
        this.ugi = connect(userId, pass, realm);
    }

    public UserGroupInformation getUgi() {
        Try.run(ugi::checkTGTAndReloginFromKeytab)
                .getOrElseThrow(t -> new RuntimeException("Unable to relogin from keytab!"));
        return ugi;
    }

    private UserGroupInformation connect(final String userId, final String pass, final String realm) {
        final File keytab = createKeytab(userId, pass, realm);
        final File krbFile = Try.of(
                () -> ResourceUtils.getFile("classpath:krb5.conf"))
                .getOrElseThrow(t -> new RuntimeException("Unable to find krb5.conf on classpath!"));
        LOGGER.debug("Using keytab file: {}", keytab.getAbsolutePath());

        return connect(userId, realm, keytab, krbFile);
    }

    private UserGroupInformation connect(final String userId, final String realm,
                                         final File keytab, final File krbFile) {
        System.setProperty(JAVA_SECURITY_KRB5_CONF, krbFile.getAbsolutePath());
        UserGroupInformation.setConfiguration(createConfiguration(realm));
        return Try.of(
                () -> UserGroupInformation.loginUserFromKeytabAndReturnUGI(buildPrincipal(userId, realm), keytab.getAbsolutePath()))
                .getOrElseThrow(t -> new RuntimeException("Failed to login user from keytab!"));
    }

    private File createKeytab(final String userId, final String pass, final String realm) {
        final File temp = createTempFile(userId);
        createKeytab(temp, buildPrincipal(userId, realm), pass);
        return temp;
    }

    private File createTempFile(final String principal) {
        final File ret = Try.of(
                () -> File.createTempFile(String.format("%s%d.keytab", principal, System.currentTimeMillis()), ".tmp"))
                .getOrElseThrow(t -> new RuntimeException("Unable to allocate temp file!"));
        ret.deleteOnExit();
        return ret;
    }

    private void createKeytab(final File keytabFile, final String principal, final String pass) {
        final Keytab keytab = new Keytab();
        final KerberosTime timestamp = new KerberosTime();

        final List<KeytabEntry> entries = new ArrayList<>();
        for (final Map.Entry<EncryptionType, EncryptionKey> e : KerberosKeyFactory.getKerberosKeys(principal, pass).entrySet()) {
            final EncryptionKey key = e.getValue();
            final byte keyVersion = (byte)key.getKeyVersion();
            entries.add(new KeytabEntry(principal, 1, timestamp, keyVersion, key));
        }
        keytab.setEntries(entries);
        Try.run(() -> keytab.write(keytabFile))
                .getOrElseThrow(t -> new RuntimeException("Unable to write keytab to temp file!"));
    }

    private static String buildPrincipal(final String userId, final String realm) {
        return userId + "@" + realm;
    }

    private static Configuration createConfiguration(final String realm) {
        final Configuration ret = new Configuration();
        ret.set("hadoop.security.authentication", "kerberos");
        ret.set("dfs.namenode.kerberos.principal.pattern", "hdfs/*@" + realm);
        return ret;
    }
}
