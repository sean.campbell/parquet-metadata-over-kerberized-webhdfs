package com.sean.webhdfsparquet.config.pass;

@FunctionalInterface
public interface PasswordLoader {
    String getPassword();
}
