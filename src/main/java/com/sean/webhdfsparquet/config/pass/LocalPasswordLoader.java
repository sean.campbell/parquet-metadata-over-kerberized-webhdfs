package com.sean.webhdfsparquet.config.pass;

public class LocalPasswordLoader implements PasswordLoader {
    private String password;

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }
}
