package com.sean.webhdfsparquet.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sean.webhdfsparquet.config.pass.PasswordLoader;
import com.sean.webhdfsparquet.provider.webhdfs.SchemaProvider;
import com.sean.webhdfsparquet.provider.webhdfs.WebHDFSSource;
import com.sean.webhdfsparquet.provider.webhdfs.util.WebHDFSClient;
import com.sean.webhdfsparquet.util.UserGroupInformationAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfiguration {

    @Bean
    public UserGroupInformationAdapter adapter(@Value("${hadoop.user}") final String userId,
                                               @Autowired final PasswordLoader loader,
                                               @Value("${hadoop.user}") final String realm) {
        return new UserGroupInformationAdapter(userId, loader.getPassword(), realm);
    }

    @Bean
    public WebHDFSClient webHDFSClient(@Autowired final UserGroupInformationAdapter adapter) {
        return new WebHDFSClient(adapter);
    }

    @Bean
    public SchemaProvider schemaProvider(@Autowired final WebHDFSClient client,
                                         @Value("${schema-source.query}") final String query) {
        return new WebHDFSSource(client, query, new ObjectMapper());
    }
}
