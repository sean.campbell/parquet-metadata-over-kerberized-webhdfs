package com.sean.webhdfsparquet.config;

import com.sean.webhdfsparquet.config.pass.LocalPasswordLoader;
import com.sean.webhdfsparquet.config.pass.PasswordLoader;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

public class PasswordLoadingConfiguration {
    @Bean
    @Primary
    @ConfigurationProperties(prefix = "hadoop")
    @Profile("!cloud")
    public PasswordLoader localLoader() {
        return new LocalPasswordLoader();
    }
}
