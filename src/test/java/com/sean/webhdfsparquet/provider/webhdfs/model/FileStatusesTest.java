package com.sean.webhdfsparquet.provider.webhdfs.model;

import com.sean.webhdfsparquet.util.JSONHelper;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class FileStatusesTest {
    private static final String FILE_STATUS_RESOURCE = "json/file-status.json",
            FILE_STATUSES_RESOURCE = "json/file-statuses.json",
            WRAPPER_RESOURCE = "json/file-statuses-wrapper.json";

    @Test
    public void validFileStatus() throws IOException {
        final FileStatus uut = JSONHelper.loadObject(FILE_STATUS_RESOURCE, FileStatus.class);

        Assert.assertNotNull(uut);
        Assert.assertNotNull(uut.getPathSuffix());
        Assert.assertNotNull(uut.getType());
        Assert.assertNotEquals(uut.getModificationTime(), 0);
    }

    @Test
    public void validFileStatuses() throws IOException {
        final FileStatuses uut = JSONHelper.loadObject(FILE_STATUSES_RESOURCE, FileStatuses.class);

        Assert.assertNotNull(uut);
        Assert.assertNotNull(uut.getFileStatus());
        Assert.assertFalse(uut.getFileStatus().isEmpty());
    }

    @Test
    public void validWrapper() throws IOException {
        final FileStatusesWrapper uut = JSONHelper.loadObject(WRAPPER_RESOURCE, FileStatusesWrapper.class);

        Assert.assertNotNull(uut);
        Assert.assertNotNull(uut.getFileStatuses());
        Assert.assertNotNull(uut.getFileStatuses().getFileStatus());
        Assert.assertFalse(uut.getFileStatuses().getFileStatus().isEmpty());

    }
}
