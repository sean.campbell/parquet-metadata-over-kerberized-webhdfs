package com.sean.webhdfsparquet.provider.webhdfs.util;

import com.sean.webhdfsparquet.util.SparkResource;
import io.vavr.control.Try;
import org.apache.commons.io.FileUtils;
import org.apache.spark.rdd.RDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.junit.*;
import parquet.bytes.BytesUtils;
import parquet.hadoop.metadata.ParquetMetadata;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Map;

public class ParquetMetadataReaderTest {
    private static final String RANDOM_SCHEMA = "{\"a\":1212, \"b\":123}";
    private static final String PARQUET_DIR = "random.parquet";

    @ClassRule
    public static SparkResource spark = new SparkResource("Parquet Metadata Reader Test");

    private ParquetMetadataReader uut;
    private Path tempDir;

    @Before
    public void before() {
        tempDir = Try.of(() -> Files.createTempDirectory(PARQUET_DIR))
                .getOrElseThrow(t -> new RuntimeException("Unable to create temp dir!"));

        uut = new ParquetMetadataReader();
    }

    @After
    public void after() {
        Try.of(() -> tempDir.toFile()).andThenTry(FileUtils::deleteDirectory);
    }

    @Test
    public void metadataFetch() {
        writeToLocalDisk(RANDOM_SCHEMA, tempDir.toAbsolutePath().toString());

        try (final InputStream is = new BufferedInputStream(new FileInputStream(getLatestParquetFile(tempDir)))) {
            final int length = is.available();
            final int footerLengthPos = uut.getFooterLengthPosition(length);

            is.mark(length);
            is.skip(footerLengthPos);

            final int footerLength = BytesUtils.readIntLittleEndian(is);
            is.reset();

            final int footerStartPos = uut.getFooterStartPosition(footerLength, length);
            is.skip(footerStartPos);

            final ParquetMetadata metadata = uut.readMetadata(is);
            Assert.assertNotNull(metadata);
            Assert.assertNotNull(metadata.getFileMetaData());

            final Map<String, String> kvMap = metadata.getFileMetaData().getKeyValueMetaData();
            Assert.assertNotNull(kvMap);
            Assert.assertFalse(kvMap.isEmpty());
            Assert.assertTrue(kvMap.containsKey("org.apache.spark.sql.parquet.row.metadata"));

        } catch (final Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test(expected = RuntimeException.class)
    public void pretendSmallFile() {
        uut.getFooterLengthPosition(4);
    }

    private static void writeToLocalDisk(final String data, final String path) {
        final RDD<String> rdd = spark.session().createDataset(
                Collections.singletonList(data), Encoders.STRING()).rdd();

        final Dataset<Row> ds = spark.session().read().json(rdd);
        ds.write().mode("overwrite").parquet(path);
    }

    private static File getLatestParquetFile(final Path dir) {
        return Try.of(() -> Files.list(dir)
                .filter(f -> f.getFileName().toString().startsWith("part") && f.getFileName().toString().contains("parquet"))
                .reduce((a, b) -> a.toFile().lastModified() <= b.toFile().lastModified() ? a : b)
                // Should always reduce to something
                .get()
        ).getOrElseThrow(t -> new RuntimeException("")).toFile();
    }
}
