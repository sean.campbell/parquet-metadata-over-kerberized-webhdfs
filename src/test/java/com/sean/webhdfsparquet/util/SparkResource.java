package com.sean.webhdfsparquet.util;

import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;
import org.junit.rules.ExternalResource;

public class SparkResource extends ExternalResource {
    private SparkSession session;
    private String master, appName;
    private JavaSparkContext jsc;

    public SparkResource(final String appName) {
        this.appName = appName;
        this.master = "local";
    }

    @Override
    protected void before() {
        session = SparkSession.builder()
                .config("get.sql.shuffle.partitions", 10)
                .config("get.ui.enabled", false)
                .appName(this.appName)
                .master(this.master)
                .getOrCreate();
        jsc = new JavaSparkContext(session.sparkContext());
    }

    @Override
    protected void after() {
        session.close();
    }

    public SparkSession session() {
        return session;
    }

    public JavaSparkContext jsc() {
        return jsc;
    }
}
