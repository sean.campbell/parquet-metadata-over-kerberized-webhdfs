package com.sean.webhdfsparquet.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Resources;

import java.io.IOException;
import java.nio.charset.Charset;

public class JSONHelper {

    public static <T> T loadObject(final String filename, final ObjectMapper mapper, final TypeReference<T> ref) throws IOException {
        return mapper.readValue(Resources.getResource(filename), ref);
    }

    public static <T> T loadObject(final String filename, final TypeReference<T> ref) throws IOException {
        return loadObject(filename, new ObjectMapper(), ref);
    }

    public static <T> T loadObject(final String filename, final ObjectMapper mapper, final Class<T> clz) throws IOException {
        return mapper.readValue(Resources.getResource(filename), clz);
    }

    public static <T> T loadObject(final String filename, final Class<T> clz) throws IOException {
        return loadObject(filename, new ObjectMapper(), clz);
    }

    public static String loadString(final String filename) throws IOException {
        return Resources.toString(Resources.getResource(filename), Charset.defaultCharset());
    }

    public static <T> String writeString(final T obj) throws JsonProcessingException {
        return writeString(obj, new ObjectMapper());
    }

    public static <T> String writeString(final T obj, final ObjectMapper mapper) throws JsonProcessingException {
        return mapper.writeValueAsString(obj);
    }

}
